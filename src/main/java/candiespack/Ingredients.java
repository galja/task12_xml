package candiespack;

public class Ingredients {
    private int sugar;
    private int syrup;
    private int milk;

    public int getSugar() {
        return sugar;
    }

    public void setSugar(int sugar) {
        this.sugar = sugar;
    }

    public int getSyrup() {
        return syrup;
    }

    public void setSyrup(int syrup) {
        this.syrup = syrup;
    }

    public int getMilk() {
        return milk;
    }

    public void setMilk(int milk) {
        this.milk = milk;
    }

    @Override
    public String toString() {
        return "Ingredients{" +
                "sugar=" + sugar +
                ", syrup=" + syrup +
                ", milk=" + milk +
                '}';
    }
}
