package parser.stax;

import candiespack.Candy;
import candiespack.Ingredients;
import candiespack.Value;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class StAXReader {
    public static List<Candy> parseCandies(File xml, File xsd){
        List<Candy> candyList = new ArrayList<>();
        Candy candy = null;
        Value value = null;
        Ingredients ingredients = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
            while(xmlEventReader.hasNext()){
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()){
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name) {
                        case "candy":
                            candy = new Candy();

                            Attribute idAttr = startElement.getAttributeByName(new QName("beerNo"));
                            if (idAttr != null) {
                                candy.setCandyId(Integer.parseInt(idAttr.getValue()));
                            }
                            break;
                        case "name":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert candy != null;
                            candy.setName(xmlEvent.asCharacters().getData());
                            break;
                        case "type":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert candy != null;
                            candy.setType(xmlEvent.asCharacters().getData());
                            break;
                        case "energy":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert candy != null;
                            candy.setEnergy(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "production":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert candy != null;
                            candy.setProduction(xmlEvent.asCharacters().getData());
                            break;
                        case "ingredients":
                            xmlEvent = xmlEventReader.nextEvent();
                            ingredients = new Ingredients();
                            break;
                        case "sugar":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert ingredients != null;
                            ingredients.setSugar(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "syrup":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert ingredients != null;
                            ingredients.setSyrup(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "milk":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert ingredients != null;
                            ingredients.setMilk(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "value":
                            xmlEvent = xmlEventReader.nextEvent();
                            value = new Value();
                            break;
                        case "carbohydrates":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert value != null;
                            value.setCarbohydrates(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "fat":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert value != null;
                            value.setFat(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "proteins":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert value != null;
                            value.setProteins(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                    }
                }

                if(xmlEvent.isEndElement()){
                    EndElement endElement = xmlEvent.asEndElement();
                    if(endElement.getName().getLocalPart().equals("beer")){
                        candyList.add(candy);
                    }
                }
            }

        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return candyList;
    }
}

