package parser.sax;

import candiespack.Candy;
import candiespack.Ingredients;
import candiespack.Value;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SAXHandler extends DefaultHandler {
    private List<Candy> candies = new ArrayList<>();
    private Candy candy = null;
    private Ingredients ingredients = null;
    private Value value = null;

    private boolean candyName = false;
    private boolean candyType = false;
    private boolean candyEnergy = false;
    private boolean candyIngredients = false;
    private boolean candySugar = false;
    private boolean candyCyrup = false;
    private boolean candyMilk = false;
    private boolean candyValue = false;
    private boolean candyProteins = false;
    private boolean candyFat = false;
    private boolean candyCarbohydrates = false;
    private boolean candyProduction = false;

    public List<Candy> getCandies() {
        return this.candies;
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("candy")) {
            String candyId = attributes.getValue("candyId");
            candy = new Candy();
            candy.setCandyId(Integer.parseInt(candyId));
        } else if (qName.equalsIgnoreCase("name")) {
            candyName = true;
        } else if (qName.equalsIgnoreCase("type")) {
            candyType = true;
        } else if (qName.equalsIgnoreCase("enery")) {
            candyEnergy = true;
        } else if (qName.equalsIgnoreCase("ingredients")) {
            candyIngredients = true;
        } else if (qName.equalsIgnoreCase("sugar")) {
            candySugar = true;
        } else if (qName.equalsIgnoreCase("syrup")) {
            candyCyrup = true;
        } else if (qName.equalsIgnoreCase("milk")) {
            candyMilk = true;
        } else if (qName.equalsIgnoreCase("value")) {
            candyValue = true;
        } else if (qName.equalsIgnoreCase("proteins")) {
            candyProteins = true;
        } else if (qName.equalsIgnoreCase("fat")) {
            candyFat = true;
        } else if (qName.equalsIgnoreCase("carbohydrates")) {
            candyCarbohydrates = true;
        } else if (qName.equalsIgnoreCase("production")) {
            candyProduction = true;
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("candy")) {
            candies.add(candy);
        }
    }

    public void characters(char ch[], int start, int length) throws SAXException {
        if (candyName) {
            candy.setName(new String(ch, start, length));
            candyName = false;
        } else if (candyType) {
            candy.setType(new String(ch, start, length));
            candyType = false;
        } else if (candyEnergy) {
            candy.setEnergy(Integer.parseInt(new String(ch, start, length)));
            candyEnergy = false;
        } else if (candyIngredients) {
            ingredients = new Ingredients();
            candyIngredients = false;
        } else if (candySugar) {
            ingredients.setSugar(Integer.parseInt(new String(ch, start, length)));
            candySugar = false;
        } else if (candyCyrup) {
            ingredients.setSyrup(Integer.parseInt(new String(ch, start, length)));
            candyCyrup = false;
        } else if (candyMilk) {
            ingredients.setSyrup(Integer.parseInt(new String(ch, start, length)));
            candyMilk = false;
        } else if (candyValue) {
            value = new Value();
            candyValue = false;
        } else if (candyProteins) {
            int proteins = Integer.parseInt(new String(ch, start, length));
            value.setProteins(proteins);
            candyProteins = false;
        } else if (candyFat) {
            int fat = Integer.parseInt(new String(ch, start, length));
            value.setFat(fat);
            candyFat = false;
        } else if (candyCarbohydrates) {
            int carbohydrates = Integer.parseInt(new String(ch, start, length));
            value.setCarbohydrates(carbohydrates);
            candyCarbohydrates = false;
        } else if (candyProduction) {
            candy.setProduction(new String(ch, start, length));
            candyProduction = false;
        }
    }
}

