package parser.sax;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import candiespack.Candy;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SAXParserCandy {
        private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

        public static List<Candy> parseCandies(File xml, File xsd){
            List<Candy> candyList = new ArrayList<>();
            try {
                saxParserFactory.setSchema(SAXValidator.createSchema(xsd));

                SAXParser saxParser = saxParserFactory.newSAXParser();
                SAXHandler saxHandler = new SAXHandler();
                saxParser.parse(xml, saxHandler);

                candyList = saxHandler.getCandies();
            }catch (SAXException | ParserConfigurationException | IOException ex){
                ex.printStackTrace();
            }

            return candyList;
        }
    }