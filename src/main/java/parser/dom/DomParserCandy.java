package parser.dom;

import java.io.File;
import java.io.IOException;
import java.util.List;

import candiespack.Candy;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class DomParserCandy {
    public static List<Candy> getCandyList(File xml, File xsd){
        DomDocCreator creator = new DomDocCreator(xml);
        Document doc = creator.getDocument();

        try {
            DomValidator.validate(DomValidator.createSchema(xsd),doc);
        }catch (IOException | SAXException ex){
            ex.printStackTrace();
        }

        DOMDocReader reader = new DOMDocReader();

        return reader.readDoc(doc);
    }
}