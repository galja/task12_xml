package parser.dom;

import candiespack.Candy;
import candiespack.Ingredients;
import candiespack.Value;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DOMDocReader {
    public List<Candy> readDoc(Document doc){
        doc.getDocumentElement().normalize();
        List<Candy> candies = new ArrayList<>();

        NodeList nodeList = doc.getElementsByTagName("candy");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Candy candy = new Candy();
            Value values;
            Ingredients ingredients;

            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE){
                Element element = (Element) node;

                candy.setCandyId(Integer.parseInt(element.getAttribute("candyId")));
                candy.setName(element.getElementsByTagName("name").item(0).getTextContent());
                candy.setType(element.getElementsByTagName("type").item(0).getTextContent());

                values = getValue(element.getElementsByTagName("value"));
                ingredients = getIngredients(element.getElementsByTagName("ingredients"));

                candy.setValue(values);
                candy.setIngredients(ingredients);
                candies.add(candy);
            }
        }
        return candies;
    }

    private Value getValue(NodeList nodes){
        Value value = new Value();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element) nodes.item(0);
            value.setCarbohydrates(Integer.parseInt(element.getElementsByTagName("carbohydrates").item(0).getTextContent()));
            value.setFat(Integer.parseInt(element.getElementsByTagName("fat").item(0).getTextContent()));
            value.setProteins(Integer.parseInt(element.getElementsByTagName("proteins").item(0).getTextContent()));
        }

        return value;
    }

    private Ingredients getIngredients(NodeList nodes){
        Ingredients ingredients = new Ingredients();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element) nodes.item(0);
            ingredients.setMilk(Integer.parseInt(element.getElementsByTagName("milk").item(0).getTextContent()));
            ingredients.setSugar(Integer.parseInt(element.getElementsByTagName("sugar").item(0).getTextContent()));
            ingredients.setSyrup(Integer.parseInt(element.getElementsByTagName("syrup").item(0).getTextContent()));
        }

        return ingredients;
    }
}
