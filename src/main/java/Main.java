import parser.dom.DomParserCandy;
import parser.sax.SAXParserCandy;
import parser.stax.StAXReader;

import java.io.File;

public class Main {

    public static void main(String[] args) {
        File file1 = new File("src/main/resources/xml_files/plane.xml");
        File file2 = new File("src/main/resources/xml_files/plane.xsd");

        DomParserCandy.getCandyList(file1, file2)
                .forEach(System.out::println);

        SAXParserCandy.parseCandies(file1, file2)
                .forEach(System.out::println);

        StAXReader.parseCandies(file1, file2).forEach(System.out::println);
    }
}